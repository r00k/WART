from .BasePlugin import BasePlugin
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions


class ScreenshotPlugin(BasePlugin):
    """
    Takes a screenshot of the target.

    """
    # Plugin Display Name
    name = "Screenshot"

    # Plugin Display Version
    version = "1"

    web_driver = None

    def __init__(self, target, output_dir):
        """
        Requires an output directory for storing screenshots.

        :param target:
        :param output_dir:
        """

        self.output_dir = output_dir

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        super().__init__(target)

    def execute(self):
        driver = self.load_driver()

        if driver is None:
            print("Could Not Load Web Driver")
            return

        try:
            driver.get(self.target.url)
            screenshot_path = "{}/{}.png".format(self.output_dir, self.target.url.replace("/", "_"))
            driver.save_screenshot(screenshot_path)
            self.results = {"driver": self.web_driver, "path": screenshot_path}

        except Exception as e:
            print(driver)
            print("Exception occurred for {} | {}".format(self.target.url, e))
            self.results = "ERROR"
            pass
        driver.close()

        # Finish by calling the base execute method to update the target
        super().execute()

    def load_driver(self):
        file_path = os.path.dirname(os.path.abspath(__file__)) + "/ScreenshotPlugin_static/webdrivers/OSX"

        # Attempt to load Chrome driver
        try:
            chrome_options = ChromeOptions()
            chrome_options.add_argument("--headless")
            driver = webdriver.Chrome(
                executable_path=f"{file_path}/chromedriver",
                chrome_options=chrome_options, service_log_path="/dev/null")
            self.web_driver = "Chrome"
            return driver
        except Exception as e:
            pass

        # Attempt to load Firefox driver
        try:
            ff_options = FirefoxOptions()
            ff_options.add_argument("--headless")
            driver = webdriver.Firefox(
                executable_path=f"{file_path}/geckodriver",
                options=ff_options, log_path="/dev/null")
            self.web_driver = "FireFox (gecko)"
            return driver
        except Exception as e:
            pass


