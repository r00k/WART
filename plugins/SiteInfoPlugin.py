from .BasePlugin import BasePlugin
import requests
import bs4


class SiteInfoPlugin(BasePlugin):
    """
    Gets basic site information:
    Title
    Headers
    """

    # Plugin Display Name
    name = "SiteInfo"

    # Plugin Display Version
    version = "1"

    def execute(self):

        r = requests.get(self.target.url)
        html = bs4.BeautifulSoup(r.text, "html.parser")

        results = {
            "title": html.title.text,
            "headers": dict(r.headers)
        }

        self.results = results

        # Finish by calling the base execute method to update the target
        super().execute()

