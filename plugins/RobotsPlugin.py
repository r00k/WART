from .BasePlugin import BasePlugin
import requests


class RobotsPlugin(BasePlugin):
    """
    Checks for a robots.txt and grabs the contents.
    """

    # Plugin Display Name
    name = "Robots"

    # Plugin Display Version
    version = "1"

    def execute(self):

        r = requests.get(self.target.url + "/robots.txt")
        if r.status_code != 200:
            self.results = "Not Found (Non 200 Response)"

        elif "allow:" not in r.text.lower():
            self.results = "Not Found (Invalid robots.txt format)"

        else:
            results = []
            for line in r.text.split("\n"):
                if "allow:" in line.lower():
                    results.append(line)

            self.results = results

        # Finish by calling the base execute method to update the target
        super().execute()

