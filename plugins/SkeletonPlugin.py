from .BasePlugin import BasePlugin


class SkeletonPlugin(BasePlugin):
    """
    Bare-bones Plugin to be used as boilerplate for writing new plugins.
    """

    # Plugin Display Name
    name = "Skeleton"

    # Plugin Display Version
    version = "0"

    def execute(self):
        """
        Plugin logic goes here. Set results instance attribute to the desired plugin output.
        results must be JSON deserializable

        :return: None
        """

        self.results = f"Skeleton Plugin Executed For Target: {self.target}"

        # Finish by calling the base execute method to update the target
        super().execute()

