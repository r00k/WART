from .BasePlugin import BasePlugin
from Wappalyzer.Wappalyzer import Wappalyzer, WebPage


class WappalyzerPlugin(BasePlugin):
    name = "Wappalyzer"
    version = "1"

    def execute(self):
        # Grab data from apps.json
        wappalyzer = Wappalyzer.latest()
        web_page = WebPage.new_from_url(self.target.url)
        self.results = list(wappalyzer.analyze(web_page))

        super().execute()




