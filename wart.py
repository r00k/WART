import json
from Target import Target
from plugins.WappalyzerPlugin import WappalyzerPlugin
from plugins.SkeletonPlugin import SkeletonPlugin
from plugins.ScreenshotPlugin import ScreenshotPlugin
from plugins.RobotsPlugin import RobotsPlugin
from plugins.SiteInfoPlugin import SiteInfoPlugin

target_urls = ["https://r00k.io", "http://example.com", "https://github.com"]

results = []

for url in target_urls:
    print(f"Beginning Target: {url}")
    target = Target(url)
    WappalyzerPlugin(target).execute()
    ScreenshotPlugin(target, "screenshots").execute()
    SkeletonPlugin(target).execute()
    RobotsPlugin(target).execute()
    SiteInfoPlugin(target).execute()
    results.append(target.dict())


print(json.dumps(results, indent=4))

f = open("example_output.json", "w")
f.write(json.dumps(results, indent=4))
f.close()
