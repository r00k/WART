class Target:

    def __init__(self, url):
        self.url = url
        self.data = {}

    def add_plugin_data(self, data):
        if "Plugins" not in self.data:
            self.data["Plugins"] = []

        self.data["Plugins"].append(data)

    def dict(self):
        output_dict = {
            "url": self.url,
            "data": self.data
        }
        return output_dict

    def __str__(self):
        return self.url
